// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: GPL-3.0-only

#ifndef CONTENTSTORE_H
#define CONTENTSTORE_H

#include <QObject>
#include "contentscope.h"

class ContentStore : public QObject
{
    Q_OBJECT
    Q_PROPERTY(ContentScope::Scope scope READ scope WRITE setScope NOTIFY scopeChanged)
    Q_PROPERTY(QString uri READ uri NOTIFY uriChanged)

public:
    explicit ContentStore(QObject *parent = nullptr);

    QString uri() const;
    QString StorageLocation() const;

    ContentScope::Scope scope() const;
    void setScope(const ContentScope::Scope scope);


Q_SIGNALS:
    void scopeChanged();
    void uriChanged();

private:
    void updateStorageLocation();

    ContentScope::Scope m_scope;

    QString m_StorageLocation;
};

#endif // CONTENTSTORE_H
