// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: GPL-3.0-only

#ifndef CONTENTHANDLER_H
#define CONTENTHANDLER_H

#include <QObject>

class ContentHandler : public QObject
{
    Q_OBJECT
public:
    enum Handler {
        Source,
        Destination,
        Share
    };
    Q_ENUM(Handler);

    explicit ContentHandler(QObject *parent = nullptr);

signals:

};

#endif // CONTENTHANDLER_H
