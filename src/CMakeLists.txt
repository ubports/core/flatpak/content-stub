add_library(contentstubplugin SHARED
    contenthandler.cpp
    content-hub-filedialog_plugin.cpp
    contentpeer.cpp
    contentstore.cpp
    contenthub.cpp
    contentitem.cpp
    contentpeermodel.cpp
    contentscope.cpp
    contenttransfer.cpp
    contenttype.cpp
    contentstub.qrc
)

target_link_libraries(contentstubplugin PRIVATE Qt5::Core Qt5::Gui Qt5::Qml Qt5::Widgets Qt5::Quick)

install(TARGETS contentstubplugin DESTINATION ${KDE_INSTALL_QMLDIR}/Ubuntu/Content/)
install(FILES qmldir.Ubuntu DESTINATION ${KDE_INSTALL_QMLDIR}/Ubuntu/Content/ RENAME qmldir)
install(TARGETS contentstubplugin DESTINATION ${KDE_INSTALL_QMLDIR}/Lomiri/Content/)
install(FILES qmldir.Lomiri DESTINATION ${KDE_INSTALL_QMLDIR}/Lomiri/Content/ RENAME qmldir)
