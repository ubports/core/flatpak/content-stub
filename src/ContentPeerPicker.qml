// SPDX-FileCopyrightText: 2020 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: GPL-3.0-only

import QtQuick 2.7
import QtQuick.Controls 2.5
import QtQuick.Controls.Suru 2.2

import Ubuntu.Content 1.1

Item {
    id: root
    anchors.fill: parent

    property var handler
    property var contentType
    property bool showTitle: true
    property ContentPeer peer
    property var customPeerModelLoader
    property string headerText
    property var completed: true

    signal peerSelected
    signal cancelPressed

    ListView {
        id: list
        anchors.fill: parent
        header: Control {
            id: control

            width: list.width
            padding: 10

            contentItem: Label {
                visible: root.showTitle
                wrapMode: Text.WordWrap
                text: root.headerText
                      ? root.headerText
                      : (root.handler === ContentHandler.Source
                         ? qsTr("Select source to import from:")
                         : qsTr("Select destination to export to:"))
            }

            background: Rectangle {
                anchors.bottom: control.bottom
                anchors.left: control.left
                anchors.right: control.right

                height: control.Suru.units.dp(1)
                color: control.Suru.neutralColor
            }
        }

        ContentPeerModel {
            id: contentPeerModel
            contentType: root.contentType
            handler: root.handler
        }

        model: contentPeerModel.peers
        delegate: ItemDelegate {
            anchors.left: parent.left
            anchors.right: parent.right
            height: 60
            text: model.name

            onClicked: {
                root.peer = modelData
                peerSelected()
            }
        }
    }
}
