// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: GPL-3.0-only

#ifndef CONTENTHUBFILEDIALOG_PLUGIN_H
#define CONTENTHUBFILEDIALOG_PLUGIN_H

#include <QQmlExtensionPlugin>

class ContentHubFiledialogPlugin : public QQmlExtensionPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID QQmlExtensionInterface_iid)

public:
    void registerTypes(const char *uri) override;
};

#endif // CONTENTHUBFILEDIALOG_PLUGIN_H
