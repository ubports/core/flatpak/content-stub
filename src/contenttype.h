// SPDX-FileCopyrightText: 2013 Canonical Ltd.
// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: GPL-3.0-only

#ifndef CONTENTTYPE_H
#define CONTENTTYPE_H

#include <QObject>

class ContentType : public QObject
{
    Q_OBJECT

public:
    enum Type {
        Undefined = -2,
        All = -1,
        Unknown = 0,
        Documents = 1,
        Pictures = 2,
        Music = 3,
        Contacts = 4,
        Videos = 5,
        Links = 6,
        EBooks = 7,
        Text = 8,
        Events = 9
    };
    Q_ENUM(Type)

    explicit ContentType(QObject *parent = nullptr);
};

#endif // CONTENTTYPE_H
