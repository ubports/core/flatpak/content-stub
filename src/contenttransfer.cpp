// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: GPL-3.0-only

#include "contenttransfer.h"

ContentTransfer::ContentTransfer(QObject *parent)
    : QObject(parent)
    , m_state(State::Created)
    , m_direction(Direction::Export)
    , m_selectionType(SelectionType::Multiple)
    , m_contentType(ContentType::All)
    , m_contentStore(nullptr)
{
}

ContentTransfer::State ContentTransfer::state() const
{
    return m_state;
}

void ContentTransfer::setState(ContentTransfer::State state)
{
    m_state = state;
    Q_EMIT stateChanged();
}

ContentTransfer::Direction ContentTransfer::direction() const
{
    return m_direction;
}

ContentTransfer::SelectionType ContentTransfer::selectionType() const
{
    return m_selectionType;
}

void ContentTransfer::setSelectionType(ContentTransfer::SelectionType selectionType)
{
    m_selectionType = selectionType;
    Q_EMIT selectionTypeChanged();
}

QQmlListProperty<ContentItem> ContentTransfer::itemsProperty()
{
    return QQmlListProperty<ContentItem>(this, m_items);
}

QString ContentTransfer::source() const
{
    return m_source;
}

void ContentTransfer::setSource(const QString &source)
{
    m_source = source;
    Q_EMIT sourceChanged();
}

QString ContentTransfer::destination() const
{
    return m_dest;
}

void ContentTransfer::setDestination(const QString &dest)
{
    m_dest = dest;
    Q_EMIT destinationChanged();
}

QList<ContentItem *> ContentTransfer::items() const
{
    return m_items;
}

void ContentTransfer::setContentType(const ContentType::Type &contentType)
{
    m_contentType = contentType;
}

void ContentTransfer::setItems(const QList<ContentItem *> &items)
{
    m_items = items;
    Q_EMIT itemsChanged();
}

bool ContentTransfer::start()
{
    if (m_state != State::Created) {
        return false;
    }
    setState(State::Initiated);

    return true;
}

bool ContentTransfer::finalize()
{
    setState(ContentTransfer::Finalized);

    return true;
}

const QString ContentTransfer::store() const
{
    if (m_contentStore) {
        return m_contentStore->uri();
    }

    return {};
}

void ContentTransfer::setStore(ContentStore *contentStore)
{
    m_contentStore = contentStore;
}

QString ContentTransfer::downloadId()
{
    return m_downloadId;
}

void ContentTransfer::setDownloadId(const QString &downloadId)
{
    m_downloadId = downloadId;
    Q_EMIT downloadIdChanged();
}

ContentType::Type ContentTransfer::contentType() const
{
    return m_contentType;
}
