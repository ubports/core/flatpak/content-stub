// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: GPL-3.0-only

#include "contentstore.h"

#include <QStandardPaths>

ContentStore::ContentStore(QObject *parent)
    : QObject(parent)
    , m_scope(ContentScope::System)
{
    updateStorageLocation();
}

QString ContentStore::uri() const
{
    // Returns the storage location for now
    return m_StorageLocation;
}

ContentScope::Scope ContentStore::scope() const
{
    return m_scope;
}

void ContentStore::setScope(const ContentScope::Scope scope)
{
    m_scope = scope;
}

inline void ContentStore::updateStorageLocation()
{
    switch(m_scope) {
    // Doesn't really make a difference in flatpak, since apps are sandboxed from each other.
    case ContentScope::System:
    case ContentScope::User:
    case ContentScope::App:
        m_StorageLocation = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
    }
}

QString ContentStore::StorageLocation() const
{
    return m_StorageLocation;
}
