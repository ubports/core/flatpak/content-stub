// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: GPL-3.0-only

#ifndef CONTENTSCOPE_H
#define CONTENTSCOPE_H

#include <QObject>

class ContentScope : public QObject
{
    Q_OBJECT
public:
    enum Scope {
        System,
        User,
        App
    };
    Q_ENUM(Scope);

    explicit ContentScope(QObject *parent = nullptr);

signals:

};

#endif // CONTENTSCOPE_H
